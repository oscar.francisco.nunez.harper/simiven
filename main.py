import sys
import csv
import time
from randomNumbers import all_numbers
from numberTest import test_csv as test_numbers
from simulation import get_simulation
from pi_simulation import get_simulation as get_pi_simulation
from pathlib import Path
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QApplication, QTableWidgetItem
from PySide2.QtCore import QFile


def reset():
    statusbar.clearMessage()
    table.setRowCount(0)
    table.setColumnCount(0)


# Se ocupa generar el csv con los numeros pseudoaleatorios con el boton
def pushButtonRandom():
    reset()
    seed = window.spinBox.value()
    all_numbers({seed})
    loadCsv("default.csv")


# Se ocupa realizar las pruebas al presionar el botton
def pushButtonTest():
    reset()
    tests = test_numbers("default.csv")
    columns = len(tests)
    table.setColumnCount(columns)
    table.setRowCount(1)
    table.setHorizontalHeaderLabels(list(tests.keys()))
    i = 0
    all_test_success = True;
    for value in tests.values():
        all_test_success = all_test_success and value
        passed = "Paso" if value else "Fallo"
        table.setItem(0, i, QTableWidgetItem(passed))
        i += 1
    if all_test_success:
        statusbar.showMessage(f'Paso todas las pruebas')
    else:
        statusbar.showMessage(f'No paso todas las pruebas') 

# Se ocupa realizar la simulacion al presionar el botton
def pushButtonSimulation():
    reset()
    table.setColumnCount(6)
    headers = [
        "R",
        "Q",
        "Costo ordenar",
        "Costo inventario",
        "Costo faltante",
        "Costo Total",
    ]
    table.setHorizontalHeaderLabels(headers)
    simulation = get_simulation()
    table.setRowCount(len(simulation))
    for row in range(0,len(simulation)):
        if row == 0:
            best_value = (row,simulation[row][-1])
        for value in range(1,len(simulation[row])):
            table.setItem(row,value, QTableWidgetItem(str(simulation[row][value])))
            if simulation[row][-1] < best_value[-1]:
                best_value = [row,simulation[row][-1]]
        table.setItem(row,0,QTableWidgetItem(str(simulation[row][1])))
        table.setItem(row,1,QTableWidgetItem(str(simulation[row][2])))
        table.setItem(row,2,QTableWidgetItem(str(simulation[row][0])))
    statusbar.showMessage(f'La mejor combinación encontrada fue R = {simulation[best_value[0]][1]} y Q = {simulation[best_value[0]][2]} en la iteracion {best_value[0]+1}')

def pushButtonGonzales():
    reset()

def pushButtonNunez():
    reset()
    simulation = get_pi_simulation("default.csv")
    table.setColumnCount(len(simulation[0]))
    table.setRowCount(len(simulation)-1)
    table.setHorizontalHeaderLabels(simulation[0])
    table.setItem(0,0, QTableWidgetItem(str(simulation[1][1])))
    for row in range(1,len(simulation)):
        for value in range(0,len(simulation[row])):
            table.setItem(row-1,value, QTableWidgetItem(str(simulation[row][value])))      
    statusbar.showMessage(f'El valor de pi aproximado es {simulation[-1][-1]}')

def pushButtonOjeda():
    reset()


def loadCsv(file):
    with open(file, "r") as csvfile:
        csvline = csv.reader(csvfile)
        headers = next(csvline)
        headerCount = 0
        for item in headers:
            headerCount += 1
            table.setColumnCount(headerCount)
        table.setHorizontalHeaderLabels(headers)
        rowCount = -1
        for row in csvline:
            rowCount += 1
            table.insertRow(rowCount)
            for column in range(0, headerCount):
                table.setItem(column, rowCount, QTableWidgetItem(row[column]))
    statusbar.showMessage(f'Se generaron {rowCount} números')



if __name__ == "__main__":
    # Se determina el folder con el fin de cargar el ui
    if getattr(sys, "frozen", False):
        folder = Path(sys._MEIPASS)
    else:
        folder = Path(__file__).parent
    uifile = str(folder) + "/main.ui"
    app = QApplication(sys.argv)

    file = QFile(uifile)
    file.open(QFile.ReadOnly)

    loader = QUiLoader()
    window = loader.load(file)

    # Ocupamos li6gar los botones a funciones
    window.pushButtonRandom.clicked.connect(pushButtonRandom)
    window.pushButtonTest.clicked.connect(pushButtonTest)
    window.pushButtonSimulation.clicked.connect(pushButtonSimulation)
    window.pushButtonGonzales.clicked.connect(pushButtonGonzales)
    window.pushButtonNunez.clicked.connect(pushButtonNunez)
    window.pushButtonOjeda.clicked.connect(pushButtonOjeda)
    table = window.tableWidget
    statusbar = window.statusbar
    window.show()

    sys.exit(app.exec_())
