Python based inventory simulation using Qt

To download dependencies:
```virtualenv .env && source .env/bin/activate && pip install -r requirements.txt```

To deploy:
```pyinstaller --hidden-import PySide2.QtXml  --add-data "main.ui;." --add-data "seasonal_factors.txt;." --add-data "delivery_time.txt;." --add-data "demand.txt;."  --onefile  main.py -w```
