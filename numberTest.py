import sys
import csv
import math


def average_test(numbers):
    n = len(numbers)
    z0 = ((sum(numbers) / n - 0.5) * math.sqrt(n)) / (math.sqrt(1 / 12))
    return (
        True if abs(z0) <= 1.96 else False
    )  # 1.96 es el valor si queremos 5% de error


def kolmogorov_smirnov_test(numbers):
    n = len(numbers)
    ascending_numbers = sorted(numbers)
    d = 0
    for i in range(1, n):
        r = i / n - ascending_numbers[i - 1]
        d = r if r > d else d
    return True if d < 1.36 / math.sqrt(n) else False


def get_numbers(file):
    numbers = set()
    with open(file, "r") as csv_file:
        csv_reader = csv.reader(csv_file)
        next(csv_reader)
        for row in csv_reader:
            numbers.add(float(row[0]))
    return numbers


def test_numbers(numbers):
    results = {"promedio": average_test(numbers)}
    results["kolmogorov"] = kolmogorov_smirnov_test(numbers)
    return results


def test_csv(csv):
    numbers = get_numbers(csv)
    return test_numbers(numbers)


if __name__ == "__main__":
    test_csv("default.csv")
