# Para escoger buenas vairables segun el libro raul coss de simulacion
# modulo m numero primo menor o igual a 18,446,744,073,709,551,616‬ en un procesador de 64 bits
# multiplicador a impar no divisible por 3 o 5, si se requiere periodo completo (a-1)mod b donde b es factor primo
# constante aditiva c mod 8 = 5, c impar y relativamente primo a m
# semilla Xo no importa
# ejemplo
# m = 18,446,744,073,709,551,357
# a = 18,446,744,073,709,551,353
# c = 18,446,744,077,


def main():
    default_question = input("Desea utilizar los valores predeterminados (N)?: ")
    if default_question.upper() == "N":
        run_user_values()
    else:
        run_default()


def run_default():
    all_numbers()
    return


def run_user_values():
    seed = int(input("Ingresa valor de la semilla X0: "))
    numbers = set(seed)
    multiplier = int(input("Ingresa el valor del multiplicador a: "))
    increment = int(input("Ingresa el valor de la constante aditiva c: "))
    modulus = int(input("Ingresa el modulo m: "))
    filename = "s{}_m{}_i{}_m{}.csv".format(seed, multiplier, increment, modulus)
    all_numbers(numbers, multiplier, increment, modulus, filename)
    return


def next_number(previous_number, multiplier, increment, modulus):
    return (multiplier * previous_number + increment) % modulus


def all_numbers(
    numbers={50000},
    multiplier=100001,
    increment=1003,
    modulus=100003,
    max=100000,
    filename="default.csv",
):
    current_number = numbers.pop()
    with open(filename, "w") as f:
        f.write("numeros \n")
        for i in range(0, max):
            current_number = next_number(current_number, multiplier, increment, modulus)
            if current_number in numbers:
                break
            f.write(str(current_number / modulus) + "\n")
            numbers.add(current_number)
    return numbers


if __name__ == "__main__":
    main()
