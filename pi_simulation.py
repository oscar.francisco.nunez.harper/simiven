import csv
import math
from itertools import repeat


def get_numbers(file):
    numbers = []
    with open(file, "r") as csv_file:
        csv_reader = csv.reader(csv_file)
        next(csv_reader)
        for row in csv_reader:
            numbers.append(float(row[0]))
    return numbers


def get_simulation(file):
    max_iterations = 10000
    results = [("numeros", "sqrt(1-X)", "pi")]
    fx = []
    numbers = get_numbers(file)
    max_iterations = len(numbers)
    for i in range(0, max_iterations):
        fx.append(4 * math.sqrt(1 - numbers[i] ** 2))
        pi_aproximation = sum(fx) / len(fx)
        results.append((numbers[i], fx[i], pi_aproximation))
        if abs(math.pi - pi_aproximation) <= 0.000001:
            return results
    return results


if __name__ == "__main__":
    print(simulation("default.csv")[-1])
